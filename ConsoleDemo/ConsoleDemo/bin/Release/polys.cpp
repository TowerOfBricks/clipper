#include<iostream>
#include<time.h>
using namespace std;

int main () {
    int polys;
    cin >> polys;
    int verts;
    cin >> verts;

    int scale;
    cin >> scale;

    cout << polys << endl;

    srand ( time(NULL) );

    for (int p=0;p<polys;p++) {

        int x0 = rand() % 1000 - 500;
        int y0 = rand() % 1000 - 500;
        int d = 50;
        cout << verts << endl;
        for (int i=0;i<verts;i++) {
            x0 += rand() % d*2 - d;
            y0 += rand() % d*2 - d;
            cout << x0*scale << ", " << y0*scale << endl;
        }
    }
}